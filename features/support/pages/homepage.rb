class HomePage
  include PageObject

  page_url 'https://www.alibaba.com/'
  link :log_in_link,xpath:".//*[@id='header2012']/div[1]/div/div/a[1]"
  in_iframe(:id=>'alibaba-login-box') do |frame|
    text_field(:email,:id=>'fm-login-id', :frame=>frame)
    text_field(:password, id:'fm-login-password',:frame=>frame)
    button(:submit_button, id:'fm-login-submit',:frame=>frame)
  end

  def log_in
    log_in_link
    self.email='milantrinh1110@gmail.com'
    self.password='B0d1p@mp'
    submit_button
  end
end