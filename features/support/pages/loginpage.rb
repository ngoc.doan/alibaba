class LoginPage
  include PageObject

  textarea :email,id:'fm-login-id'
  textarea :password, id:'fm-login-password'
  button :submit_button, id:'fm-login-submit'

  def log_in
    wait_until { email_element.visible? }
    self.email.set('milantrinh1110@gmail.com')
    self.password.set('B0d1p@mp')
    submit_button
  end
end